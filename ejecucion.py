import build.build_run as file_build
import config.init_config as file_config
import os
import configparser

#Lectura de archivo de configuracion para conexion a DB
config = configparser.ConfigParser()
config.read('config_db.ini')
db_nombre = config['DB']['db']
db_driver = config['DB']['driver']
db_server = config['DB']['server']
db_user = config['DB']['user']
db_password = config['DB']['password']

#Devuelve ruta de ubicacion
path = os.path.dirname(os.path.realpath(__file__))

#Llamado de archivos necesario para ejecucion completa
#Dependiendo el insumo requerido se envia la informacion necesaria
#path es ruta del archivo insumo en este caso Datos.xlsx
file_config.config_db_tables(driver = db_driver, server=db_server, db=db_nombre, user=db_user, password=db_password)
file_build.exe_upload_data_and_sp(driver = db_driver, server=db_server, db=db_nombre, user=db_user, password=db_password,path_file=path)

