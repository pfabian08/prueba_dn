import pyodbc
import os

def abrir_archivos(archivo):
    f = open(archivo,'r')
    return f.read()

def config_db_tables(driver, server, db, user, password):

    #Conexion a DB
    try:
        conexion = pyodbc.connect(''.join(['DRIVER={',driver,
            '};SERVER=',server,
            ';DATABASE=',db,
            ';UID=',user,';PWD=',password,
            ';Trusted_Connection=yes']))
    except Exception as e:
        print("Ocurrió un error al conectar a SQL Server Express: ", e)

    #Lectura y ejecucion de archivos sql
    files_sql = {
        0:"create_table.sql",
        #1:"config_database.sql", #se deja comentada en vista que no se pudo ejecutar desde aca pero si en SSMS
        1:"stored_procedure_insumo1.sql",
        2:"stored_procedure_insumo2.sql"
    }
    path = os.path.dirname(os.path.realpath(__file__))
    try:
        with conexion.cursor() as exe:
            for i in range(len(files_sql)):
                exe.execute(abrir_archivos(path+'\\'+files_sql[i]))
    except Exception as e:
        print("Ocurrió un error al insertar: ", e)
    finally:
        conexion.close()

if __name__ == "__main__":
    print('DB Configurada')
    config_db_tables()