IF NOT EXISTS(select * FROM sys.tables where name = 'INDICADOR_VISITAS')
begin
CREATE TABLE INDICADOR_VISITAS
(
    SubjectID INT
    ,DPEncargado VARCHAR(MAX)
    ,EstadoTicket VARCHAR(MAX)
    ,JobId VARCHAR(MAX)
    ,Id_propuesta INT
    ,Gerente VARCHAR(MAX)
    ,Muestra INT
    ,PaisEjecucion	VARCHAR(MAX)
    ,PaisVenta VARCHAR(MAX)
    ,TipoEstudio VARCHAR(MAX)
    ,Tecnica VARCHAR(MAX)
    ,Plataforma VARCHAR(MAX)
    ,FechaInicioEstimada SMALLDATETIME
    ,FechaFinEstimada SMALLDATETIME
    ,Programa VARCHAR(MAX)
    ,Entrevistador VARCHAR(MAX)
    ,DiaCarga SMALLDATETIME
    ,Dia TINYINT
    ,Mes TINYINT
    ,Ano SMALLINT
    ,EstatusEncuesta VARCHAR(MAX)
    ,Test VARCHAR(MAX)
    ,VisitStart SMALLDATETIME
    ,VisitEnd SMALLDATETIME
    ,QualityControlFlag	VARCHAR(MAX)
    ,IsFiltered	BIT
    ,Cancelada	BIT
    ,Expirada	BIT
    ,Validadas	BIT
    ,GPSValidation	BIT
    ,Descartadas	BIT
    ,ClientDuration	TIME
    ,TiempoEntreEncuesta	INT
    ,DuracionMinutos	SMALLINT
    ,IncidenciaFiltro	INT
    ,CuentasClave VARCHAR(MAX)

)
END