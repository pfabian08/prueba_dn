CREATE OR ALTER PROCEDURE [Insumo1]
	@v_PaisEjecucion as VARCHAR(MAX) = null
	,@v_Anho as SMALLINT = null
	,@v_Mes as TINYINT = null
AS
BEGIN

SET NOCOUNT ON

DECLARE @TablaConteo TABLE (PaisEjecucion VARCHAR(MAX), Entrevistador VARCHAR(MAX), Mes TINYINT, Ano SMALLINT, GPSValidation FLOAT,
	Descartadas FLOAT, Expirada FLOAT, Validadas FLOAT, Cancelada FLOAT)

INSERT INTO @TablaConteo 
	SELECT PaisEjecucion
		,Entrevistador
		,Mes
		,Ano
		,CAST(COUNT(CASE
				WHEN GPSValidation = 1 THEN 1
				ELSE NULL
			END) AS FLOAT) AS GPSValidation
		,CAST(COUNT(
			CASE
				WHEN Descartadas = 0 THEN 1
				ELSE NULL
			END) AS FLOAT) as Descartadas
		,CAST(COUNT(
			CASE
				WHEN Expirada = 1 THEN 1
				ELSE NULL
			END) AS FLOAT) as Expirada
		,CAST(COUNT(
			CASE
				WHEN Validadas = 1 THEN 1
				ELSE NULL
			END) AS FLOAT) as Validadas
		,CAST(COUNT(
			CASE
				WHEN Cancelada = 1 THEN 1
				ELSE NULL
			END) AS FLOAT) as Cancelada
FROM INDICADOR_VISITAS
GROUP BY Entrevistador, PaisEjecucion, Mes, Ano

DECLARE @TablaIndicador TABLE (PaisEjecucion VARCHAR(MAX), Entrevistador VARCHAR(MAX), Mes TINYINT, Ano SMALLINT, IndicadorGPS FLOAT,
	IndicadorExpiradas FLOAT, Canceladas FLOAT)

INSERT INTO @TablaIndicador
	SELECT PaisEjecucion
		,Entrevistador
		,Mes
		,Ano
		,IIF(Descartadas = 0,
			IIF(GPSValidation = 0,NULL,100),
			(GPSValidation/Descartadas)*100) AS IndicadorGPS
		,IIF(Validadas = 0,
			IIF(Expirada = 0,NULL,0),
			(1-(Expirada/Validadas))*100) AS IndicadorExpiradas
		,IIF(Validadas = 0,
			IIF(Cancelada = 0,NULL,0),
			(1-(Cancelada/Validadas))*100) AS Canceladas
	FROM @TablaConteo

DECLARE @TablaCalidad TABLE (PaisEjecucion VARCHAR(MAX), Entrevistador VARCHAR(MAX), Mes TINYINT, Ano SMALLINT, GPS FLOAT,
	Expiradas FLOAT, Canceladas FLOAT)

INSERT INTO @TablaCalidad
	SELECT PaisEjecucion
		,Entrevistador
		,Mes
		,Ano
		,CASE
			WHEN IndicadorGPS < 70 THEN 0
			WHEN IndicadorGPS > 89 THEN 25
			WHEN IndicadorGPS is null THEN NULL
			ELSE 89 - IndicadorGPS
		END AS GPS
		,CASE
			WHEN IndicadorExpiradas < 85 THEN 0
			WHEN IndicadorExpiradas is null THEN NULL
			ELSE (IndicadorExpiradas - 85) * 1.575
		END AS Expiradas
		,CASE
			WHEN Canceladas = 100 THEN 50
			WHEN Canceladas is null THEN NULL
			ELSE 0
		END AS Canceladas
	FROM @TablaIndicador

SELECT PaisEjecucion
	,Mes
	,Ano
	,AVG(GPS) as AVG_GPS
	,AVG(Expiradas) as AVG_Expiradas
	,AVG(Canceladas) as AVG_Canceladas
FROM @TablaCalidad
GROUP BY PaisEjecucion, Mes, Ano
HAVING (PaisEjecucion = @v_PaisEjecucion or @v_PaisEjecucion is null)
	AND (Ano = @v_Anho or @v_Anho is null)
	AND (Mes = @v_Mes or @v_Mes is null)

END