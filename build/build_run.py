import pyodbc
import pandas as pd
import os

def exe_upload_data_and_sp(driver, server, db, user, password, path_file = '', file="Datos.xlsx", nro_sp=1, PaisEjecucion = 'NULL', Ano = 'NULL', Mes = 'NULL'):

    #Conexion DB
    try:
        conexion = pyodbc.connect(''.join(['DRIVER={',driver,
            '};SERVER=',server,
            ';DATABASE=',db,
            ';UID=',user,';PWD=',password,
            ';Trusted_Connection=yes']))
        print("Conexion OK")
    except Exception as e:
        print("Ocurrió un error al conectar a SQL Server Express: ", e)

    #Lectura archivo excel (Insumo)
    file = "Datos.xlsx"
    path = os.path.dirname(os.path.realpath(__file__))
    df = pd.read_excel(''.join([path_file,'\\',file]))

    #Conversion excel a csv para subir a MSSQL
    NewFile = 'Datos_convert.csv'
    pathNewFile = ''.join([path,"\\",NewFile])
    df.to_csv(pathNewFile,index=False,encoding='utf-8-sig')

    sp = "Insumo"+str(nro_sp)

    #Ejecucion de archivos SQL
    try:
        with conexion.cursor() as exe:
            exe.execute("TRUNCATE TABLE INDICADOR_VISITAS")
            exe.execute(f"""
            BULK INSERT INDICADOR_VISITAS
            FROM '{pathNewFile}'
            WITH (FIRSTROW = 2,
            CODEPAGE = '65001',
            FIELDTERMINATOR= ',',
            ROWTERMINATOR = '\n')
            """)
            #Dependiendo el Insumo requerido lo ejecuta
            if nro_sp == 1:
                sql = f"EXECUTE {sp} @v_PaisEjecucion = {PaisEjecucion}, @v_Anho = {Ano}, @v_Mes = {Mes}"
            else:
                sql = f"EXECUTE {sp} @v_PaisEjecucion = {PaisEjecucion}"
            print(sql)
            data = pd.read_sql(sql,conexion)
            print(data)
    except Exception as e:
        print("Ocurrió un error al intentar ejecutar consultas: ", e)
    finally:
        conexion.close()

if __name__ == "__main__":
    print('Data Subida y generada')
    exe_upload_data_and_sp()

